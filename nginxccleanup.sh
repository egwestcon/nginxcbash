#!/bin/bash
curl -k -b session --request DELETE \
  --url https:///nginxc.westcon.com/api/v1/services/environments/production/apps/arcadia_main_app/components/arcadia_main_app_comp
sleep 10
curl -k -b session --request DELETE \
  --url https:///nginxc.westcon.com/api/v1/services/environments/production/gateways/arcadia_gw
sleep 10
curl -k -b session --request DELETE \
  --url https:///nginxc.westcon.com/api/v1/services/environments/production/apps/arcadia_main_app
