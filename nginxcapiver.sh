#!/bin/bash
curl -k -b session --request PUT \
  --url https:///nginxc.westcon.com/api/v1/services/api-definitions/arcadia_api/versions/2.0.1-schema \
  --header 'content-type: application/json' \
  --data '{
  "metadata": {
    "name": "2.0.1-schema",
    "displayName": "API Arcadia Finance",
    "description": "Arcadia OpenAPI",
    "tags": []
  },
  "desiredState": {
    "specs": {
      "REST": {
        "openapi": "3.0.0",
        "info": {
          "description": "Arcadia OpenAPI",
          "title": "API Arcadia Finance",
          "version": "2.0.1-schema"
        },
        "paths": {
          "/api/rest/execute_money_transfer.php": {
            "x-controller-match-method": "EXACT",
            "post": {
              "summary": "Transfer money to a friend",
              "requestBody": {
                "required": true,
                "content": {
                  "application/json": {
                    "schema": {
                      "properties": {
                        "account": {
                          "format": "int64",
                          "type": "integer"
                        },
                        "amount": {
                          "format": "int64",
                          "type": "integer"
                        },
                        "currency": {
                          "type": "string"
                        },
                        "friend": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "amount",
                        "account",
                        "currency",
                        "friend"
                      ],
                      "type": "object"
                    },
                    "example": {
                      "account": "2075894",
                      "amount": "92",
                      "currency": "GBP",
                      "friend": "Vincent"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "200 response",
                  "content": {
                    "application/json": {
                      "example": {
                        "currency": "GBP",
                        "msg": "The money transfer has been successfully completed",
                        "name": "Vincent",
                        "status": "success",
                        "transid": "524569855"
                      }
                    }
                  }
                }
              }
            }
          },
          "/trading/rest/buy_stocks.php": {
            "x-controller-match-method": "EXACT",
            "post": {
              "summary": "Add stocks to your portfolio",
              "requestBody": {
                "required": true,
                "content": {
                  "application/json": {
                    "schema": {
                      "properties": {
                        "action": {
                          "type": "string"
                        },
                        "company": {
                          "type": "string"
                        },
                        "qty": {
                          "format": "int64",
                          "type": "integer"
                        },
                        "stock_price": {
                          "format": "int64",
                          "type": "integer"
                        },
                        "trans_value": {
                          "format": "int64",
                          "type": "integer"
                        }
                      },
                      "required": [
                        "trans_value",
                        "qty",
                        "company",
                        "action",
                        "stock_price"
                      ],
                      "type": "object"
                    },
                    "example": {
                      "action": "buy",
                      "company": "MSFT",
                      "qty": "16",
                      "stock_price": "198",
                      "trans_value": "312"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "200 response",
                  "content": {
                    "application/json": {
                      "example": {
                        "amount": "312",
                        "name": "Microsoft",
                        "qty": "16",
                        "status": "success",
                        "transid": "855415223"
                      }
                    }
                  }
                }
              }
            }
          },
          "/trading/rest/sell_stocks.php": {
            "x-controller-match-method": "EXACT",
            "post": {
              "summary": "Sell stocks that you own",
              "requestBody": {
                "required": true,
                "content": {
                  "application/json": {
                    "schema": {
                      "properties": {
                        "action": {
                          "type": "string"
                        },
                        "company": {
                          "type": "string"
                        },
                        "qty": {
                          "format": "int64",
                          "type": "integer"
                        },
                        "stock_price": {
                          "format": "int64",
                          "type": "integer"
                        },
                        "trans_value": {
                          "format": "int64",
                          "type": "integer"
                        }
                      },
                      "required": [
                        "trans_value",
                        "qty",
                        "company",
                        "action",
                        "stock_price"
                      ],
                      "type": "object"
                    },
                    "example": {
                      "action": "sell",
                      "company": "MSFT",
                      "qty": "16",
                      "stock_price": "158",
                      "trans_value": "212"
                    }
                  }
                }
              },
              "responses": {
                "200": {
                  "description": "200 response",
                  "content": {
                    "application/json": {
                      "example": {
                        "amount": "212",
                        "name": "Microsoft",
                        "qty": "16",
                        "status": "success",
                        "transid": "658854124"
                      }
                    }
                  }
                }
              }
            }
          },
          "/trading/transactions.php": {
            "x-controller-match-method": "EXACT",
            "get": {
              "summary": "Get the latests transactions that have happened",
              "responses": {
                "200": {
                  "description": "200 response",
                  "content": {
                    "application/json": {
                      "example": {
                        "YourLastTransaction": "MFST 2000"
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}'
