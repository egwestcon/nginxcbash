#!/bin/bash

curl -k -c session --request POST \
  --url https:///nginxc.westcon.com/api/v1/platform/login \
  --header 'content-type: application/json' \
  --data '{"credentials":{"type":"BASIC","password":"P@ssw0rd","username":"bob.westcon@gmail.com"}}'
