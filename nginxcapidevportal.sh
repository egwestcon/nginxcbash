#!/bin/bash
curl -k -b session --request PUT \
  --url https:///nginxc.westcon.com/api/v1/services/environments/production/devportals/arcadia_dev_portal \
  --header 'content-type: application/json' \
  --data '{
  "metadata": {
    "name": "arcadia_dev_portal",
    "tags": [],
    "kind": "devportal"
  },
  "desiredState": {
    "ingress": {
      "gatewayRefs": [
        {
          "ref": "/services/environments/production/gateways/arcadia_dev_portal"
        }
      ]
    },
    "devPortalTheme": {
      "customConfig": {
        "fonts": {
          "assignments": {
            "headings": {
              "kind": "google-web-font",
              "value": "Lato"
            },
            "body": {
              "kind": "google-web-font",
              "value": "Fira+Sans"
            },
            "code": {
              "kind": "google-web-font",
              "value": "IBM+Plex+Mono"
            },
            "cta": {
              "kind": "google-web-font",
              "value": "Lato"
            },
            "special": {
              "kind": "google-web-font",
              "value": "Lato"
            }
          }
        },
        "primary": {
          "color": {
            "primary": "#3b43be",
            "accent": "#3b43be",
            "fill": "#ffffff",
            "ink": "#171d21"
          }
        },
        "secondary": {
          "color": {
            "primary": "#3b43be",
            "accent": "#ffffff",
            "fill": "#f7f8fa",
            "ink": "#2c3039",
            "gray": "#272938"
          }
        }
      }
    },
    "publishedApiRefs": [
      {
        "ref": "/services/environments/production/apps/arcadia_main/published-apis/arcadia_api_pub"
      }
    ],
    "devPortalType": "private"
  }
}'



