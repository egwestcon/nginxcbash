#!/bin/bash
curl -k -b session --request PUT \
  --url https:///nginxc.westcon.com/api/v1/services/environments/production/apps/arcadia_main/published-apis/arcadia_api_pub \
  --header 'content-type: application/json' \
  --data '{
  "metadata": {
    "name": "arcadia_api_pub",
    "tags": []
  },
  "desiredState": {
    "apiDefinitionVersionRef": {
      "ref": "/services/api-definitions/arcadia_api/versions/2.0.1-schema"
    },
    "gatewayRefs": [
      {
        "ref": "/services/environments/production/gateways/arcadia_api_gw"
      }
    ],
    "devportalRefs": [
      {
        "ref": "/services/environments/production/devportals/arcadia_dev_portal"
      }
    ],
    "basePath": "/",
    "stripWorkloadBasePath": false
  }
}'


