#!/bin/bash
curl -k -b session --request PUT \
  --url https:///nginxc.westcon.com/api/v1/services/api-definitions/arcadia_api \
  --header 'content-type: application/json' \
  --data '{
  "metadata": {
    "name": "arcadia_api",
    "tags": []
  }
}'
