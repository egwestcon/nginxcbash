#!/bin/bash
curl -k -b session --request POST \
  --url https:///nginxc.westcon.com/api/v1/services/environments/production/apps/arcadia_main_app/components \
  --header 'content-type: application/json' \
  --data '{
  "metadata": {
    "name": "arcadia_main_app_comp",
    "displayName": "Arcadia Main Application Comp",
    "tags": []
  },
  "desiredState": {
    "backend": {
      "ntlmAuthentication": "DISABLED",
      "preserveHostHeader": "DISABLED",
      "workloadGroups": {
        "arcadia_sg_wlg": {
          "locationRefs": [
            {
              "ref": "/infrastructure/locations/production"
            }
          ],
          "loadBalancingMethod": {
            "type": "ROUND_ROBIN"
          },
          "uris": {
            "https://arcadia.westcon.com/": {
              "isBackup": false,
              "isDown": false,
              "isDrain": false
            }
          }
        }
      },
      "monitoring": {
        "defaultState": "HEALTHY",
        "uri": "/"
      }
    },
    "ingress": {
      "gatewayRefs": [
        {
          "ref": "/services/environments/production/gateways/arcadia_gw"
        }
      ],
      "uris": {
        "https://arcadia.westcon.com": {
          "tls": {
            "certRef": {
              "ref": "/services/environments/production/certs/arcadia_certificate"
            },
            "preferServerCipher": "DISABLED",
            "sessionCache": "OFF"
          }
        }
      }
    },
    "logging": {
      "accessLog": {
        "state": "DISABLED"
      },
      "errorLog": "DISABLED"
    },
    "security": {
      "strategyRef": {
        "ref": "/services/strategies/balanced_default"
      },
      "waf": {
        "isEnabled": true,
        "isMonitorOnly": false,
        "signatureOverrides": {}
      }
    }
  }
}'