#!/bin/bash
curl -k -b session --request PUT \
  --url https:///nginxc.westcon.com/api/v1/services/environments/production/apps/arcadia_main/components/arcadia_api_comp \
  --header 'content-type: application/json' \
  --data '{
  "metadata": {
    "createTime": "2022-01-19T04:06:35Z",
    "kind": "components",
    "links": {
      "rel": "/api/v1/services/environments/production/apps/arcadia_main/components/arcadia_api_comp"
    },
    "name": "arcadia_api_comp",
    "tags": [],
    "uid": "cd407c92-a412-4ed6-8738-785da3062cb3",
    "updateTime": "2022-01-19T04:06:35Z"
  },
  "desiredState": {
    "backend": {
      "ntlmAuthentication": "DISABLED",
      "preserveHostHeader": "DISABLED",
      "workloadGroups": {
        "arcadia_api_wlg": {
          "loadBalancingMethod": {
            "type": "ROUND_ROBIN"
          },
          "locationRefs": [
            {
              "links": {
                "displayName": "Production Env",
                "name": "production",
                "rel": "/api/v1/infrastructure/locations/production"
              },
              "ref": "/infrastructure/locations/production"
            }
          ],
          "uris": {
            "https://api.westcon.com": {
              "isBackup": false,
              "isDown": false,
              "isDrain": false
            }
          }
        }
      }
    },
    "ingress": {
      "gatewayRefs": [
        {
          "ref": "/services/environments/production/gateways/arcadia_api_gw"
        }
      ],
      "uris": {
        "/api/rest/execute_money_transfer.php": {
          "matchMethod": "EXACT"
        },
        "/trading/rest/buy_stocks.php": {
          "matchMethod": "EXACT"
        },
        "/trading/rest/sell_stocks.php": {
          "matchMethod": "EXACT"
        },
        "/trading/transactions.php": {
          "matchMethod": "EXACT"
        }
      }
    },
    "logging": {
      "accessLog": {
        "state": "DISABLED"
      },
      "errorLog": "DISABLED"
    },
    "publishedApiRefs": [
      {
        "ref": "/services/environments/production/apps/arcadia_main/published-apis/arcadia_api_pub"
      }
    ],
    "security": {
      "strategyRef": {
        "ref": "/security/strategies/balanced_default"
      },
      "waf": {
        "isEnabled": true
      }
    }
  }
}'

