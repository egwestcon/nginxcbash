#!/bin/bash

curl -k -b session --request POST \
  --url https:///nginxc.westcon.com/api/v1/services/environments/production/apps \
  --header 'content-type: application/json' \
  --data '{"metadata": {"name": "arcadia_main_app","displayName": "Arcadia Main Application","description": "","tags": []},"desiredState": {}}'