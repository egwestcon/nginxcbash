#!/bin/bash
curl -k -b session --request POST \
  --url https:///nginxc.westcon.com/api/v1/services/environments/production/gateways \
  --header 'content-type: application/json' \
  --data '{
  "metadata": {
    "name": "arcadia_gw",
    "displayName": "Arcadia Gateway",
    "tags": []
  },
  "desiredState": {
    "ingress": {
      "uris": {
        "https://arcadia.westcon.com": {}
      },
      "methods": [
        "POST",
        "GET",
        "PUT",
        "DELETE",
        "PATCH",
        "HEAD",
        "TRACE",
        "OPTIONS",
        "CONNECT"
      ],
      "placement": {
        "instanceRefs": [
          {
            "ref": "/infrastructure/locations/production/instances/nginxplus1804",
            "listenIps": [
              "192.168.200.235"
            ]
          }
        ]
      },
      "tls": {
        "certRef": {
          "ref": "/services/environments/production/certs/arcadia_certificate"
        },
        "preferServerCipher": "DISABLED"
      }
    }
  }
}'
